from django.db import models

class Person_registration(models.Model):
    person_name = models.CharField(max_length=255,null=True)
    person_phone = models.BigIntegerField(null=True,blank=True)
    person_email = models.EmailField(null=True,blank=True)

