from django.urls import path # calls path from django.urls module 

from .views.myapp0_view import Person_registration_create, Person_registration_details, Person_registration_list, Person_registration_update # call the view classes here

urlpatterns = [
    path('register/', Person_registration_create.as_view(), name='person-registration'),
    path('update/', Person_registration_update.as_view(), name='person-update'),
    path('details/', Person_registration_details.as_view(), name='person-details'),
    path('list/', Person_registration_list.as_view(), name='person-list'),
]


