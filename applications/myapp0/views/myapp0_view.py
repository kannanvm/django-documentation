from django.views.generic import CreateView, DetailView, ListView, UpdateView
from applications.myapp0.models.myapp0_model import Person_registration
from django.shortcuts import redirect, get_object_or_404
from django.urls import reverse_lazy

# Class to create person entry using CreateView
class Person_registration_create(CreateView):
    model = Person_registration
    template_name = "person_registration_create.html"
    fields = "__all__"
    def form_valid(self, form): # function to work on form valid state
        report = form.save()    # to save form data
        person_id=form.instance.id   # getting personid
        self.request.session['person_id'] = person_id  # saving in session
        return redirect('person-details') # redirecting to details view


# Person detail view
class Person_registration_details(DetailView):
    model = Person_registration
    template_name = "person_registration_details.html"
    context_object_name = 'person'

    def get_object(self): # to get object with pk with person id
        return get_object_or_404(self.model, pk=self.request.session['person_id'])

    def post(self, request):  # Taking post request contents from the template
        person_id = request.POST.get("id") # Getting person id from template through post
        self.request.session['person_id'] = person_id # Saving to session
        return redirect('person-update')



# Class to update person entry using update view
class Person_registration_update(UpdateView):
    model = Person_registration
    template_name = "person_registration_update.html"
    fields = "__all__"

    def get_object(self):
        return get_object_or_404(self.model, pk=self.request.session['person_id'])

    def form_valid(self, form):
        report = form.save()
        person_id=form.instance.id
        self.request.session['person_id'] = person_id
        return redirect('person-details')


# Class to show registration list
class Person_registration_list(ListView):
    model = Person_registration
    template_name = "person_registration_list.html"
    paginate_by = 10  # if pagination is desired

    def post(self, request):  # Taking post request contents from the template
        person_id = request.POST.get("id")
        self.request.session['person_id'] = person_id # Saving to session
        return redirect('person-details')
